package pm.gui;

import java.io.IOException;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventType;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.shape.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javax.imageio.ImageIO;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    static final String CLASS_MAX_PANE = "max_pane";
    static final String CLASS_BORDER_PANE = "bordered_pane";
    static final String CLASS_RENDER_CANVAS = "render_canvas";
    static final String CLASS_HEADER = "subheading_label";
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
   
    VBox leftPane;
 
    HBox one;
    Button selectButton;
    Button removeButton;
    Button squareButton;
    Button circleButton;
    
    HBox two;
    Button upsidedownTriangle;
    Button uprightTriangle;
    
    VBox three;
    Label label3;
    ColorPicker colorPicker3;
    
    VBox four;
    Label label4;
    ColorPicker colorPicker4;
    
    VBox five;
    Label label5;
    ColorPicker colorPicker5;
    
    VBox six;
    Label label6;
    Slider slider6;
    
    HBox seven;
    Button cameraButton;
    
    Pane rightPane;
    
    Rectangle r;
    Ellipse c;
    
    
    Rectangle rect;
    
    Shape selected;
    
    Shape selected2;
    
    Paint oldColor;

   ArrayList shapes;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
       // this.<error> = new Button("../images/SelectionTool.png");
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        ArrayList shapes = new ArrayList();
        
        one = new HBox();
        selectButton = new Button("",new ImageView("File:C:./images/SelectionTool.png"));
        one.getChildren().add(selectButton);
        selectButton.setOnAction(e -> {
            rightPane.setCursor(Cursor.DEFAULT);
            squareButton.setDisable(false);
            circleButton.setDisable(false);
            upsidedownTriangle.setDisable(false);
            uprightTriangle.setDisable(false);
            removeButton.setDisable(false);
            rightPane.setOnMouseClicked(event ->
                {
                   //getSelectedRectangle((Rectangle) event.getSource());
                });
                });
        removeButton = new Button("",new ImageView("File:C:./images/Remove.png"));
        one.getChildren().add(removeButton);
        removeButton.setOnAction(e -> {
            rightPane.getChildren().remove(selected);
        }); 
        squareButton = new Button("",new ImageView("File:C:./images/Rect.png"));
        one.getChildren().add(squareButton);
        squareButton.setOnAction(e -> {
            rightPane.setCursor(Cursor.CROSSHAIR);
           circleButton.setDisable(false);
           squareButton.setDisable(true);
           upsidedownTriangle.setDisable(true);
           uprightTriangle.setDisable(true);
           removeButton.setDisable(true);
           rightPane.setOnMousePressed(event ->{
              if (squareButton.isDisable())
                { 
                    r = new Rectangle();
                    r.setOnMouseClicked(z -> {
                        if (!squareButton.isDisable() && ! circleButton.isDisable())
                            getSelectedRectangle((Rectangle)z.getSource());
                    });
                 r.setX(event.getX());
                 r.setY(event.getY());
                 rightPane.getChildren().add(r);
                }
            }); 
           rightPane.setOnMouseDragged(t ->{
               if (squareButton.isDisable())
                { 
                 r.setFill(Color.WHITE);
                 r.setStroke(Color.BLACK);
                 r.setWidth(t.getX() - r.getX());
                 r.setHeight(t.getY() - r.getY());
                }
            });
           rightPane.setOnMouseReleased(z->{
               shapes.add(r);
           });
        });
        circleButton = new Button("",new ImageView("File:C:./images/Ellipse.png"));
        one.getChildren().add(circleButton);
        circleButton.setOnAction(e -> {
            rightPane.setCursor(Cursor.CROSSHAIR);
            squareButton.setDisable(false);
            circleButton.setDisable(true);
            upsidedownTriangle.setDisable(true);
            uprightTriangle.setDisable(true);
            removeButton.setDisable(true);
            rightPane.setOnMousePressed(event -> {
                if (circleButton.isDisable())
                { 
                 c = new Ellipse();
                 c.setOnMouseClicked(z ->{
                     if (!circleButton.isDisable() && !squareButton.isDisable())
                         getSelectedEllipse((Ellipse) z.getSource());
                    });
                 c.setCenterX(event.getX());
                 c.setCenterY(event.getY());
                 rightPane.getChildren().add(c);
                }
            });
            rightPane.setOnMouseDragged(t -> {
                if (circleButton.isDisable())
                { 
                 c.setFill(Color.WHITE);
                 c.setStroke(Color.BLACK);
                 c.setRadiusX(t.getX() - c.getCenterX());
                 c.setRadiusY(t.getY() - c.getCenterY());
                }
            });rightPane.setOnMouseReleased(z->{
               shapes.add(c);
        });
        });
        
        two = new HBox();
        upsidedownTriangle = new Button("",new ImageView("File:C:./images/MoveToBack.png"));
        two.getChildren().add(upsidedownTriangle);
        upsidedownTriangle.setOnAction(event->{
           selected.toBack();
        });
        uprightTriangle = new Button("",new ImageView("File:C:./images/MoveToFront.png"));
        two.getChildren().add(uprightTriangle);
        uprightTriangle.setOnAction(event->{
           selected.toFront();
        });
        
        three = new VBox();
        label3 = new Label("Background Color");
        three.getChildren().add(label3);
        colorPicker3 = new ColorPicker();
        three.getChildren().add(colorPicker3);
        colorPicker3.setOnAction(e -> {
                Color value = colorPicker3.getValue();
                String colorString = value.toString();
                String substring = colorString.substring(2, colorString.length()-2);
                rightPane.setStyle("-fx-background-color: linear-gradient(from 100% 100% to 100% 100%, #" + substring + ", #000000)");
                        });
        
        four = new VBox();
        label4 = new Label("Fill Color");
        four.getChildren().add(label4);
        colorPicker4 = new ColorPicker();
        four.getChildren().add(colorPicker4);
        colorPicker4.setOnAction(e->{
            Paint value = (Color) colorPicker4.getValue();
                selected.setFill(value);
        });
        
        five = new VBox();
        label5 = new Label("Outline Color");
        five.getChildren().add(label5);
        colorPicker5 = new ColorPicker();
        five.getChildren().add(colorPicker5);
        colorPicker5.setOnAction(e ->{
            Paint value = (Color) colorPicker5.getValue();
            selected.setStroke(value);
            oldColor = colorPicker5.getValue();
        });
        
        six = new VBox();
        label6 = new Label("Outline Thickness");
        six.getChildren().add(label6);
        slider6 = new Slider();
        slider6.setMin(1);
        slider6.setMax(7);
        six.getChildren().add(slider6);
        slider6.setOnMouseDragged(e->{
                selected.setStrokeWidth(slider6.getValue());
        });
        
        seven = new HBox();
        cameraButton = new Button("",new ImageView("File:./images/Snapshot.png"));
        seven.getChildren().add(cameraButton);
        cameraButton.setOnAction(event ->{
           WritableImage image = rightPane.snapshot(new SnapshotParameters(), null);
           File file = new File("Pose.png");
           try {
             ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
           } catch (IOException e) {
               System.out.println("error");
           }
        });
        
        leftPane = new VBox();
        leftPane.getChildren().addAll(one, two, three, four, five, six, seven);
        
        workspace = new BorderPane();
        ((BorderPane) workspace).setLeft(leftPane);
        
        rightPane = new Pane();
        ((BorderPane) workspace).setCenter(rightPane);
        
    }
    
    public void getSelectedRectangle(Rectangle selection)
    {
       selected = selection;
       colorPicker4.setValue((Color) selected.getFill());
       colorPicker5.setValue((Color) selected.getStroke());
       slider6.setValue(selected.getStrokeWidth());
        if (selected2 != null)
        {
            selected2.setStroke(oldColor);
        }
        oldColor = selected.getStroke();
        selected.setStroke(Color.YELLOW);
        selected2 = selected;
        selection.setOnMouseDragged(event ->{
           if (!circleButton.isDisable() && !squareButton.isDisable())
           {
                selection.setX(event.getX());
                selection.setY(event.getY());
           }
        });
    }
    public void getSelectedEllipse(Ellipse selection)
    {
       selected = selection;
       colorPicker4.setValue((Color) selected.getFill());
       colorPicker5.setValue((Color) selected.getStroke());
       slider6.setValue(selected.getStrokeWidth());
        if (selected2 != null)
        {
            selected2.setStroke(oldColor);
        }
        oldColor = selected.getStroke();
        selected.setStroke(Color.YELLOW);
        selected2 = selected;
        selection.setOnMouseDragged(event ->{
           if (!circleButton.isDisable() && !squareButton.isDisable())
           {
                selection.setCenterX(event.getX());
                selection.setCenterY(event.getY());
           }
        });
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        leftPane.getStyleClass().add(CLASS_MAX_PANE);
        one.getStyleClass().add(CLASS_BORDER_PANE);
        two.getStyleClass().add(CLASS_BORDER_PANE);
        three.getStyleClass().add(CLASS_BORDER_PANE);
        four.getStyleClass().add(CLASS_BORDER_PANE);
        five.getStyleClass().add(CLASS_BORDER_PANE);
        six.getStyleClass().add(CLASS_BORDER_PANE);
        seven.getStyleClass().add(CLASS_BORDER_PANE);
        label3.getStyleClass().add(CLASS_HEADER);
        label4.getStyleClass().add(CLASS_HEADER);
        label5.getStyleClass().add(CLASS_HEADER);
        label6.getStyleClass().add(CLASS_HEADER);
        rightPane.getStyleClass().add(CLASS_RENDER_CANVAS);
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {

    }
}
